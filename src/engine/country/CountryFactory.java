package engine.country;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/* Country Flyweight Factory */

public class CountryFactory {
	
	private HashMap<String, Country> countryMap;
	private static CountryFactory countriesFlyFactory;
	
	// Flyweight Factory private constructor
	private CountryFactory(ArrayList<Country> countries) {
		this.countryMap = new HashMap<String,Country>();
		for ( Country country : fakeCountryDataService.countries )
			countryMap.put(country.getName(), country);
	}
	
	// Flyweight Factory initializer
	public static CountryFactory getInstance() {
		if ( CountryFactory.countriesFlyFactory != null)
			return CountryFactory.countriesFlyFactory;
		
		CountryFactory.countriesFlyFactory = new CountryFactory(fakeCountryDataService.countries);
		return CountryFactory.countriesFlyFactory;
	}
	
	
	// getCountry from the flyweight factory
	public Country getCountry(String countryName) { return this.countryMap.get(countryName); }
	
	// Return the Visa Tax amount from an amount according to a country
	public float  getVisaCost(String countryName, float amount) {
		Country cnt  = this.getCountry(countryName);
		if ( cnt != null ) 
			return (cnt.getVisaTax() * Math.abs(amount));
		else
			return ( this.getCountry("OTHERLAND").getVisaTax() * Math.abs(amount) );
	}
	

}


/* FAKE DATA IN ORDER TO MOKE DATA FETCHED FROM DEMOGRAPHY SERVER OR WHATEVER :
 
- Espagne : 0,10 %
- Etats-Unis : 0,20 %
- Portugal : 0,15 %
- Royaume-Uni : 0,25 %
- Reste du monde : 0,30 % */

class fakeCountryDataService{
	public static ArrayList<Country> countries = new ArrayList<Country>(Arrays.asList(
				new Country("SPAIN", 	1.4f, 2, 0.001f),
				new Country("USA",  	1.4f, 2, 0.002f),
				new Country("PORTUGAL",	1.4f, 2, 0.0015f),
				new Country("UK",	1.4f, 2, 0.0025f),
				new Country("OTHERLAND", 1.4f, 2, 0.003f),
				new Country("FRANCE", 1.4f, 2, 0.0f)
			));
	
}



