package engine.country;

public class Country {
	private float gdp; 		  // Billions dollars
	private String name; 	  // Country name
	private int nbInhabitant; // Nb inhabitant
	private float visaTax;	  // Price of Visa payment in percent of total transaction 	
	
	public Country ( String name, float gdp, int  nbInhabitant, float visaTax) {
		this.gdp = Math.abs(gdp);
		this.name = name;
		this.nbInhabitant = Math.abs(nbInhabitant);
		this.visaTax = Math.abs(visaTax);
	}
	
	public float getGDP() {
		return this.gdp;
	}
	
	public float getVisaTax() {
		return this.visaTax;
	}
	
	public int getNbInhabitant() {
		return this.nbInhabitant;
	}
	
	public String toString() {
		return this.name;
	}
	
	public String getName(){
		return this.name;
	}
	
}
