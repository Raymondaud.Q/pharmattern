package engine.employe;

public class Address {
	
	String streetName;
	int houseNumber;
	String additional;
	String cityName;
	int postalCode;
	
	public Address(	String streetName,
					int houseNumber, 
					String cityName, 
					int postalCode) {
		
		this.streetName = streetName;
		this.houseNumber = houseNumber;
		this.cityName = cityName;
		this.postalCode = postalCode;
		
	}
	
	public Address(	String streetName,
			int houseNumber, 
			String cityName, 
			int postalCode,
			String additional) {
		
		this.streetName = streetName;
		this.houseNumber = houseNumber;
		this.cityName = cityName;
		this.postalCode = postalCode;
		this.additional = additional;

	}
	
	public String getStreetName(){ return this.streetName; }
	public int getHouseNumber(){ return this.houseNumber; 	}
	public String getCityName(){ return this.cityName; }
	public int getPostalCode() { return this.postalCode; }
	public String getAdditional() { return this.additional; }
	
}
