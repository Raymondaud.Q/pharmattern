package engine.employe;

import java.util.Calendar;
import java.util.Date;

public class OrderPicker extends Employee {
	
	int nbHour;
	int hourlyRate;
	Date contractBeginDate;

	public OrderPicker(	String name,
						String firstName,
						Address address,
						int nbHour,
						int hourlyRate) {
		super(name, firstName, address);
		this.nbHour = nbHour;
		this.hourlyRate = hourlyRate; 
		this.contractBeginDate = new Date();
		// TODO Auto-generated constructor stub
	}
	
	// Overload with date arg
	public OrderPicker(	String name,
			String firstName,
			Address address,
			int nbHour,
			int hourlyRate,
			Date contractBegin) {
		super(name, firstName, address);
		this.nbHour = nbHour;
		this.hourlyRate = hourlyRate; 
		this.contractBeginDate = contractBegin;
		// TODO Auto-generated constructor stub
	}

	@Override
	public float getSalary() {
		float advantage = 0;
		Calendar a1 = Calendar.getInstance();
		a1.setTime(this.contractBeginDate);
		a1.add(Calendar.YEAR, 3);
		if ( a1.getTime().before(new Date())) {
			advantage = 0.10f;
			a1.add(Calendar.YEAR, 3);
			if ( a1.getTime().before(new Date()))
				advantage = 0.15f;
		}
		float salary =   (this.nbHour * this.hourlyRate) * 4;
		return salary  + salary * advantage;
	}

}
