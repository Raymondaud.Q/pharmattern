package engine.employe;

public class Pharmacist extends Employee implements Seller{
	
	float fixedSalary;
	int salesCnt;

	public Pharmacist(	String name, 
						String firstName,
						Address address,
						float fixedSalary) {
		super(name, firstName, address);
		this.fixedSalary = fixedSalary;
		this.salesCnt = 0;
		// TODO Auto-generated constructor stub
	}

	@Override
	public float getSalary() {
		// Return pharmacist salary = fixed + a percent of nbSales
		return this.fixedSalary + ( salesCnt / 100 );
	}

	@Override
	public void sell() {
		this.salesCnt++;		
	}

}
