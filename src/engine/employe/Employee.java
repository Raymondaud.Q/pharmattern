package engine.employe;

public abstract class Employee implements Remunerable {
	String name;
	String firstName;
	Address address;
	
	public Employee(String name, String firstName, Address address) {
		this.name = name.toUpperCase();
		this.firstName = firstName;
		this.address = address;
	}
	
	public String getName() { return this.name; }
	public String getFirstName() { return this.firstName; }
	public Address getAddress() { return this.address; }
	public String toString() { return this.firstName+ " "+ this.name;};
	public float getSalary() { return -1; }
}
