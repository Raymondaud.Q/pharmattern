package engine.inventory;

import java.util.Hashtable;

public class OrderCommand extends InventoryCommand {
	
	public OrderCommand(Hashtable<String, ProductInventoryRecord> inventory, ProductInventoryRecord params) {
		super(inventory, params);
	}

	@Override
	public void undo() {
		ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());
		
		if (pir != null) {
			float cost = _params._count * _params._product.getBuyPrice();
			
			_params._bankAccount.credit(cost);
			pir._count -= _params._count;
		}		
	}

	@Override
	public boolean execute() {
		float cost = _params._count * _params._product.getBuyPrice();
		
		if (_params._bankAccount.debit(cost)) {
			ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());
			
			if (pir != null) {
				pir._count += _params._count;
			}
			else {
				_inventory.put(
					_params._product.getBarcode(), 
					_params
				);
			}
			
			return true;
		}
		
		return false;
	}
	
}
