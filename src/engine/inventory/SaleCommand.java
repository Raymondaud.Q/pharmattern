package engine.inventory;

import java.util.Hashtable;

public class SaleCommand extends InventoryCommand {

	public SaleCommand(Hashtable<String, ProductInventoryRecord> inventory, ProductInventoryRecord params) {
		super(inventory, params);
	}

	@Override
	public void undo() {
		ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());
		
		if (pir != null) {
			pir._count -= _params._count;
		}
	}

	@Override
	public boolean execute() {
		ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());

		// TODO: Add code for checking bank account before buying!
		
		if (pir != null) {
			pir._count -= _params._count;
		}
		else {
			return false;
		}
		
		return true;
	}
	
}
