package engine.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class ProductRepository {
	
	private static ArrayList<ISellable> _products = 
		new ArrayList<ISellable>(
			Arrays.asList(
				new Product(
					"abcdef", 
					"Produit beaut� #1", 
					ProductType.BEAUTY, 
					1.0f, 
					2.0f, 
					new Date(System.currentTimeMillis())),
				new Product(
					"bcdefg", 
					"Produit g�n�rique #1", 
					ProductType.GENERAL, 
					2.0f, 
					3.0f, 
					new Date(System.currentTimeMillis())),
				new Product(
					"cdefgh", 
					"M�dicament #1", 
					ProductType.MEDICATION, 
					4.0f, 
					5.0f, 
					new Date(System.currentTimeMillis())),
				new Product(
					"defghi", 
					"Suppl�ment alimentaire #1", 
					ProductType.NUTRITIONAL_SUPPLEMENT, 
					6.0f, 
					7.0f, 
					new Date(System.currentTimeMillis())),
				new Product(
					"efghij", 
					"Vaccin #1", 
					ProductType.VACCINE, 
					8.0f, 
					9.0f, 
					new Date(System.currentTimeMillis()))
			)
		);
	
	public static ArrayList<ISellable> getProducts() {
		return _products;
	}
	
	public static ISellable getProduct(String barcode) {
		for(ISellable product : _products) {
			if (product.getBarcode().equals(barcode)) {
				return product;
			}
		}
		
		return null;
	}
	
}
