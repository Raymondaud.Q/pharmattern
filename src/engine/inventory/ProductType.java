package engine.inventory;

public enum ProductType {
	GENERAL,
	VACCINE,
	MEDICATION,
	BEAUTY,
	NUTRITIONAL_SUPPLEMENT
}
