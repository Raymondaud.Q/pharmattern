package engine.inventory;

import java.util.Hashtable;

import engine.bank.BankSystem;

public class SaleCommandById extends InventoryCommand {

	public SaleCommandById(Hashtable<String, ProductInventoryRecord> inventory, ProductInventoryRecord params) {
		super(inventory, params);
	}

	@Override
	public void undo() {
		ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());
		
		if (pir != null) {
			pir._count -= _params._count;
		}
	}

	@Override
	public boolean execute() {
		ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());
		
		if (pir != null) {
			// Pas assez de produits en inventaire!
			if (_params._count > pir._count) {
				return false;
			}
			else {
				float cost = _params._count * _params._product.getSellPrice();
				
				BankSystem.getInstance().operate(
					"MASTER", 
					""+BankSystem.getInstance().createAccount(),
					""+_params._bankAccountUsed,
					cost);
				pir._count -= _params._count;
				
				return true;
			}
		}
		
		return false;
	}
	
}
