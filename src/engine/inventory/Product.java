package engine.inventory;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Product implements ISellable {
	
	// TODO: Create DefaultProductBuilder & PharmaCareProductBuilder to set sale prices.

	protected DecimalFormat _df = new DecimalFormat("#.00 $");
	
	private String _barcode;
	private String _name;
	private ProductType _type;
	private float _priceBuy; // Fixe pour toutes les pharmacies.
	private float _priceSell; // Peut-�tre diff�rent pour chaque pharmacie.
	private Date _expirationDate;

	@Override
	public String getBarcode() {
		return _barcode;
	}
	
	@Override
	public String getName() {
		return _name;
	}

	@Override
	public ProductType getProductType() {
		return _type;
	}

	@Override
	public float getBuyPrice() {
		return _priceBuy;
	}

	@Override
	public float getSellPrice() {
		return _priceSell;
	}
	
	public Date getExpirationDate() {
		return _expirationDate;
	}
	
	public Product(
		String barcode,
		String name, 
		ProductType type, 
		float buyPrice, 
		float sellPrice, 
		Date expirationDate) 
	{
		_barcode = barcode;
		_name = name;
		_type = type;
		_priceBuy = buyPrice;
		_priceSell = sellPrice;
		_expirationDate = expirationDate;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("======================================================================\n");
		sb.append("                 Nom: " + _name + "\n");
		sb.append("                Type: " + _type.toString() + "\n");
		sb.append("        Prix d'achat: " + _df.format(_priceBuy) + "\n");
		sb.append("       Prix de vente: " + _df.format(_priceSell) + "\n");
		sb.append("   Date d'expiration: " + new SimpleDateFormat("yyyy-MM-dd").format(_expirationDate) + " $\n");
		sb.append("======================================================================\n");
		
		return sb.toString();
	}
	
	@Override
	public String toStringLineItem() {
		return "" + _type.toString() + " : " + _name + " - " + _df.format(_priceBuy) + " - " + _df.format(_priceSell) + " - " + new SimpleDateFormat("yyyy-MM-dd").format(_expirationDate);
	}
	
}
