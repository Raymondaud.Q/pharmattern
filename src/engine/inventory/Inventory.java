package engine.inventory;

import java.util.Hashtable;
import java.util.UUID;

import engine.phil.bank.BankAccount;

public class Inventory {
	
	private Hashtable<String, ProductInventoryRecord> _productsInventory;
	private Hashtable<UUID, InventoryCommand> _inventoryEntries;
	
	public Inventory() {
		_productsInventory = new Hashtable<String, ProductInventoryRecord>();
		_inventoryEntries = new Hashtable<UUID, InventoryCommand>();
	}
	
	/***
	 * Fonction pour placer des commandes qui utilise finalement notre design 
	 * consolid� au lieu du design "alternatif" des comptes bancaires dans 
	 * Pharmarcy.
	 * 
	 * @param barcode
	 * @param amountOrdered
	 * @param bankAccountId
	 * @return
	 ***/
	public UUID placeOrderById(String barcode, int amountOrdered, int bankAccountId) {
		// On utilise un "mock" d�p�t de produits pour cette d�mo.
		// On aurait aussi pu l'ajouter en Depencendy Injection au constructeur
		// avec une interface IProductRepository.
		ISellable p = ProductRepository.getProduct(barcode);
		
		if (p == null) {
			return null;
		}
		
		InventoryCommand command = new OrderByIdCommand(_productsInventory, new ProductInventoryRecord(p, amountOrdered, bankAccountId));
		
		if (command.execute()) {
			UUID id = UUID.randomUUID();
			_inventoryEntries.put(id, command);			
			return id;
		}
		else {
			return null;
		}
	}
	
	public UUID placeOrder(String barcode, int amountOrdered, BankAccount bankAccount) {
		// On utilise un "mock" d�p�t de produits pour cette d�mo.
		// On aurait aussi pu l'ajouter en Depencendy Injection au constructeur
		// avec une interface IProductRepository.
		ISellable p = ProductRepository.getProduct(barcode);
		
		if (p == null) {
			return null;
		}
		
		InventoryCommand command = new OrderCommand(_productsInventory, new ProductInventoryRecord(p, amountOrdered, bankAccount));
		
		if (command.execute()) {
			UUID id = UUID.randomUUID();
			_inventoryEntries.put(id, command);			
			return id;
		}
		else {
			return null;
		}
	}
	
	public void cancelOrder(UUID orderId) {
		InventoryCommand command = _inventoryEntries.get(orderId);
		
		if (command != null) {
			command.undo();
			_inventoryEntries.remove(orderId);
		}
	}
	
	/***
	 * Fonction pour placer des commandes qui utilise finalement notre design 
	 * consolid� au lieu du design "alternatif" des comptes bancaires dans 
	 * Pharmarcy.
	 * 
	 * @param barcode
	 * @param amountSold
	 * @param bankAccount
	 * @return
	 */
	public UUID sellProductById(String barcode, int amountSold, int bankAccountId) {
		// On utilise un "mock" d�p�t de produits pour cette d�mo.
		// On aurait aussi pu l'ajouter en Depencendy Injection au constructeur
		// avec une interface IProductRepository.
		ISellable p = ProductRepository.getProduct(barcode);
		
		if (p == null) {
			return null;
		}
		
		InventoryCommand command = new SaleCommandById(_productsInventory, new ProductInventoryRecord(p, amountSold, bankAccountId));
		
		if (command.execute()) {
			UUID id = UUID.randomUUID();
			_inventoryEntries.put(id, command);		
			return id;
		}
		else {
			return null;
		}
	}
	public UUID sellProduct(String barcode, int amountSold, BankAccount bankAccount) {
		// On utilise un "mock" d�p�t de produits pour cette d�mo.
		// On aurait aussi pu l'ajouter en Depencendy Injection au constructeur
		// avec une interface IProductRepository.
		ISellable p = ProductRepository.getProduct(barcode);
		
		if (p == null) {
			return null;
		}
		
		InventoryCommand command = new SaleCommand(_productsInventory, new ProductInventoryRecord(p, amountSold, bankAccount));
		
		if (command.execute()) {
			UUID id = UUID.randomUUID();
			_inventoryEntries.put(id, command);		
			return id;
		}
		else {
			return null;
		}
	}
	
	public void returnProduct(UUID orderId) {
		InventoryCommand command = _inventoryEntries.get(orderId);
		
		if (command != null) {
			command.undo();
			_inventoryEntries.remove(orderId);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("======================================================================\n");
		for(ProductInventoryRecord pir : _productsInventory.values()) {
			sb.append("  > " + pir._count + "x " + pir._product.toStringLineItem() + "\n");
		}
		sb.append("======================================================================\n");
		
		return sb.toString();
	}
}
