package engine.inventory;

import java.util.Hashtable;

import engine.bank.BankSystem;

public class OrderByIdCommand extends InventoryCommand {
	
	public OrderByIdCommand(Hashtable<String, ProductInventoryRecord> inventory, ProductInventoryRecord params) {
		super(inventory, params);
	}

	@Override
	public void undo() {
		ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());
		
		if (pir != null) {
			float cost = _params._count * _params._product.getBuyPrice();
			
			BankSystem.getInstance().operate(
				"MASTER", 
				""+_params._bankAccountUsed,
				""+BankSystem.getInstance().createAccount(),
				cost);
			pir._count -= _params._count;
		}		
	}

	@Override
	public boolean execute() {
		float cost = _params._count * _params._product.getBuyPrice();
		
		if (BankSystem.getInstance().operate(
				"MASTER", 
				""+BankSystem.getInstance().createAccount(),
				""+_params._bankAccountUsed,
				cost)) {
			ProductInventoryRecord pir = _inventory.get(_params._product.getBarcode());
			
			if (pir != null) {
				pir._count += _params._count;
			}
			else {
				_inventory.put(
					_params._product.getBarcode(), 
					_params
				);
			}
			
			return true;
		}
		
		return false;
	}
	
}
