package engine.inventory;

import engine.phil.bank.BankAccount;

public class ProductInventoryRecord {
	
	public ISellable _product;
	public int _count;
	public BankAccount _bankAccount;
	public int _bankAccountUsed;
	
	public ProductInventoryRecord(ISellable product, int count, BankAccount bankAccount) {
		_product = product;
		_count = count;
		_bankAccount = bankAccount;
	}
	
	public ProductInventoryRecord(ISellable product, int count, int bankAccount) {
		_product = product;
		_count = count;
		_bankAccountUsed = bankAccount;
	}
	
	public ProductInventoryRecord(ISellable product, int count, BankAccount bankAccount, int bankAccountUsed) {
		_product = product;
		_count = count;
		_bankAccountUsed = bankAccountUsed;
	}
	
}
