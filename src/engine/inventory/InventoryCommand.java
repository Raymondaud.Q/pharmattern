package engine.inventory;

import java.util.Hashtable;

public abstract class InventoryCommand {

	protected Hashtable<String, ProductInventoryRecord> _inventory;
	protected ProductInventoryRecord _params;
	
	public InventoryCommand(Hashtable<String, ProductInventoryRecord> inventory, ProductInventoryRecord params) {
		_inventory = inventory;
		_params = params;
	}
	
	public abstract void undo();
	public abstract boolean execute();

}
