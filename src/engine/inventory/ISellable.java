package engine.inventory;

public interface ISellable {
	String getBarcode();
	String getName();
	ProductType getProductType();
	float getBuyPrice();
	float getSellPrice();
	
	String toStringLineItem();
}
