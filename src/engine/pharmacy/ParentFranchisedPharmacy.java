package engine.pharmacy;

import java.util.ArrayList;

public class ParentFranchisedPharmacy extends FranchisedPharmacy {

	private ArrayList<FranchisedPharmacy> _dependantPharmacies = new  ArrayList<FranchisedPharmacy>();
	
	public ParentFranchisedPharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		super(name, businessSIRET, commercialSurfaceArea);
	}
	
	public void addDependant(FranchisedPharmacy dependant) {
		_dependantPharmacies.add(dependant);
		
		// Calculer le nouveau rabais de franchisé.
		_franchisedBankAccount.setDiscount(this.getFranchiseDiscount());
	}
	
	@Override
	protected int getTotalDependants() {
		int totalDependants = _dependantPharmacies.size();

		for (FranchisedPharmacy fp : _dependantPharmacies) {
			totalDependants += fp.getTotalDependants();
		}
		
		return totalDependants;
	}

	@Override
	public String getPharmacyType() {
		return "Franchisé - Parent";
	}
	
	@Override
	public float getRevenues() {
		float revenues = getMonthlySales();
		
		for (FranchisedPharmacy fp : _dependantPharmacies) {
			revenues += fp.getRevenues();
		}
		
		return revenues;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("======================================================================\n");
		sb.append("                  SIRET: " + _businessSIRET + "\n");
		sb.append("                    Nom: " + _name + "\n");
		sb.append("                   Type: " + getPharmacyType() + "\n");
		sb.append("    Surface commerciale: " + _commercialSurfaceArea + " m2\n");
		sb.append(" Solde Compte Classique: " + _df.format(_primaryBankAccount.getBalance()) + "\n");
		sb.append(" Solde Compte Franchisé: " + _df.format(_franchisedBankAccount.getBalance()) + "\n");
		sb.append("      Ventes mensuelles: " + _df.format(getMonthlySales()) + "\n");
		sb.append("      Rabais sur achats: " + getFranchiseDiscount() + " %\n");
		sb.append("     Chiffre d'affaires: " + _df.format(getRevenues()) + "\n");
		sb.append("      Versement mensuel: " + getMonthlyFranchisePayment() + "\n");
		sb.append("\n");
		sb.append(" Dépendants:\n");
		for (FranchisedPharmacy fp : _dependantPharmacies) {
			sb.append("  > " + fp.toStringSingleLine() + "\n");
		}
		sb.append("======================================================================\n");
		
		return sb.toString();
	}
	
}
