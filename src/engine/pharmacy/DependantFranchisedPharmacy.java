package engine.pharmacy;

public class DependantFranchisedPharmacy extends FranchisedPharmacy {
	
	public DependantFranchisedPharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		super(name, businessSIRET, commercialSurfaceArea);
	}

	@Override
	protected int getTotalDependants() {
		return 0;
	}
	
	@Override
	public String getPharmacyType() {
		return "Franchisé - Dépendant";
	}
	
	@Override
	public float getRevenues() {
		return getMonthlySales();
	}
	
}
