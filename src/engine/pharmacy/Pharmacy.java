package engine.pharmacy;

import java.text.DecimalFormat;

import engine.phil.bank.BankAccount;
import engine.phil.bank.ClassicBankAccount;
import engine.phil.bank.FranchisedBankAccount;

public abstract class Pharmacy {

	protected DecimalFormat _df = new DecimalFormat("#.00 $");
	
	protected String _name;
	protected String _businessSIRET;
	protected float _commercialSurfaceArea;
	protected int _employeeCount;
	protected BankAccount _primaryBankAccount;
	protected int _primaryBankAccountId;

	public BankAccount getPrimaryBankAccount() {
		return _primaryBankAccount;
	}

	public int getPrimaryBankAccountId() {
		return _primaryBankAccountId;
	}
	
	public void setPrimaryBankAccountId(int value) {
		_primaryBankAccountId = value;
	}
	
	public Pharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		_name = name;
		_businessSIRET = businessSIRET;
		_commercialSurfaceArea = commercialSurfaceArea;
		_primaryBankAccount = new ClassicBankAccount();
	}
	
	public abstract String getPharmacyType();
	public abstract float getRevenues();

	public String toStringSingleLine() {
		return "" + _businessSIRET + " : " + _name + " - " + _commercialSurfaceArea + " m2 - " + _df.format(getRevenues());
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("======================================================================\n");
		sb.append("                  SIRET: " + _businessSIRET + "\n");
		sb.append("                    Nom: " + _name + "\n");
		sb.append("                   Type: " + getPharmacyType() + "\n");
		sb.append("    Surface commerciale: " + _commercialSurfaceArea + " m2\n");
		sb.append(" Solde Compte Classique: " + _df.format(_primaryBankAccount.getBalance()) + "\n");
		sb.append("     Chiffre d'affaires: " + _df.format(getRevenues()) + "\n");
		sb.append("======================================================================\n");
		
		return sb.toString();
	}
}
