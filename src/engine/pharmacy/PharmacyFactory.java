package engine.pharmacy;

public class PharmacyFactory {
	
	private static PharmacyFactory _instance;
	
	private PharmacyFactory() {
		
	}
	
	public static PharmacyFactory getInstance() {
		if (_instance == null) {
            _instance = new PharmacyFactory();
        }
        
        return _instance;
	}
	
	public Pharmacy getIndependantPharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		return new IndependantPharmacy(name, businessSIRET, commercialSurfaceArea);
	}

	public FranchisedPharmacy getParentFranchisedPharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		return new ParentFranchisedPharmacy(name, businessSIRET, commercialSurfaceArea);
	}
	
	public FranchisedPharmacy getDependantFranchisedPharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		return new DependantFranchisedPharmacy(name, businessSIRET, commercialSurfaceArea);
	}
	
}
