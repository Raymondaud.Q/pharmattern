package engine.pharmacy;

public class IndependantPharmacy extends Pharmacy {
	
	public IndependantPharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		super(name, businessSIRET, commercialSurfaceArea);
	}
	
	@Override
	public String getPharmacyType() {
		return "Indépendant";
	}
	
	@Override
	public float getRevenues() {
		return 10000.0f;
	}
	
}
