package engine.pharmacy;

import engine.phil.bank.BankAccount;
import engine.phil.bank.FranchisedBankAccount;

public abstract class FranchisedPharmacy extends Pharmacy {

	protected FranchisedBankAccount _franchisedBankAccount;
	protected int _franchisedBankAccountId;
	
	public FranchisedBankAccount getFranchisedBankAccount() {
		return _franchisedBankAccount;
	}

	public int getFranchisedBankAccountId() {
		return _franchisedBankAccountId;
	}
	
	public void setFranchisedBankAccount(int value) {
		_franchisedBankAccountId = value;
	}
	
	public FranchisedPharmacy(String name, String businessSIRET, float commercialSurfaceArea) {
		super(name, businessSIRET, commercialSurfaceArea);
		
		_franchisedBankAccount = new FranchisedBankAccount();
	}

	protected abstract int getTotalDependants();
	
	protected float getMonthlySales() {
		return 10000.0f;
	}
	
	/***
	 * L'�nonc� n'�tait pas tr�s clair pour le rabais sur l'achat puisque tel
	 * qu'il est �crit, toutes les pharmacies auraient le m�me rabais que la
	 * pharmacie au top de la pyramide. Il n'est pas pr�cis� si c'est celles
	 * qu'elle g�re DIRECTEMENT...
	 * 
	 * "7,5 % en moins sur le prix d�achat des produits [POUR LES PHARMACIES 
	 * QU'ELLES G�RENT]."
	 ***/
	public float getFranchiseDiscount() {
		int totalDependants = getTotalDependants();
		float percent = 0.0f;
		
		if (totalDependants >= 2 && totalDependants <= 4) {
			percent = 0.025f;
		}
		else if (totalDependants >= 5 && totalDependants <= 10) {
			percent = 0.05f;
		}
		else if (totalDependants > 10) {
			percent = 0.07f;
		}
		
		return percent;
	}
	
	public float getMonthlyFranchisePayment() {
		int totalDependants = getTotalDependants();
		float percent = 0.0f;
		
		if (totalDependants >= 2 && totalDependants <= 4) {
			percent = 0.01f;
		}
		else if (totalDependants >= 5 && totalDependants <= 10) {
			percent = 0.02f;
		}
		else if (totalDependants > 10) {
			percent = 0.03f;
		}
		
		return getRevenues() * percent;
	}
	
	@Override
	public String getPharmacyType() {
		return "Franchis�";
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("======================================================================\n");
		sb.append("                  SIRET: " + _businessSIRET + "\n");
		sb.append("                    Nom: " + _name + "\n");
		sb.append("                   Type: " + getPharmacyType() + "\n");
		sb.append("    Surface commerciale: " + _commercialSurfaceArea + " m2\n");
		sb.append(" Solde Compte Classique: " + _df.format(_primaryBankAccount.getBalance()) + "\n");
		sb.append(" Solde Compte Franchis�: " + _df.format(_franchisedBankAccount.getBalance()) + "\n");
		sb.append("      Ventes mensuelles: " + _df.format(getMonthlySales()) + "\n");
		sb.append("      Rabais sur achats: " + getFranchiseDiscount() + " %\n");
		sb.append("     Chiffre d'affaires: " + _df.format(getRevenues()) + "\n");
		sb.append("      Versement mensuel: " + _df.format(getMonthlyFranchisePayment()) + "\n");
		sb.append("======================================================================\n");
		
		return sb.toString();
	}
	
}
