package engine.bank;

import java.util.HashMap;

import engine.bank.network.Operable;
import engine.bank.network.Refundable;

public class BankSystem {
	private int nbAccount;
	HashMap<String,BankAccount> accounts;
	HashMap<String,Operable> bankistery;
	
	static BankSystem centralBank;
	
	private BankSystem(){
		this.nbAccount = 0;
		this.accounts  =  new HashMap<String,BankAccount>();
		this.bankistery  =  new HashMap<String,Operable>();
	}
	
	public static  BankSystem getInstance() {
		if ( centralBank != null )
			return BankSystem.centralBank;
		BankSystem.centralBank = new BankSystem();
		return BankSystem.centralBank;
	}
	
	public boolean operate(String opName, String cred, String deb, float value  ) {
		Creditable c= this.accounts.get(cred);
		Debitable d= this.accounts.get(deb);
		if ( c != null & d != null )
			return this.bankistery.get(opName).operate(c, d, value);
		return false;
	}
	
	public boolean refund(String opName, String cred, String deb, float value ) {
		Creditable c= this.accounts.get(cred);
		Debitable d= this.accounts.get(deb);
		Operable op = this.bankistery.get(opName);
		try {
			if (  op instanceof Refundable )
				return ((Refundable)op).refund(c, d, value);
			System.out.println("CHEH ! Votre opérateur ne vous remboursera pas !");
			return false;
		}
		catch (Exception e) {
			System.out.println("CHEH ! Votre opérateur ne vous remboursera pas !");
			return false;
		}
	}
	
	public boolean addNetwork(String opName, Operable operator) {
		if ( operator == null || opName == "")
			return false;
		this.bankistery.put(opName, operator);
		return true;
	}
	
	public int createAccount(String countryName) {
		this.nbAccount++;
		this.accounts.put(""+nbAccount,new BankAccount(countryName));
		return this.nbAccount;
	}
	
	public int createAccount() {
		this.nbAccount++;
		this.accounts.put(""+nbAccount,new BankAccount());
		return this.nbAccount;
	}

	public float getMoney(String string) {
		return this.accounts.get(string).getBalance();
		
	}

}
