package engine.bank;

public class BankAccount implements Debitable, Creditable{
	private String location;
	private float balance;
	
	public BankAccount(String location) {
		balance = 1000; // C'est de l'argent magique
		this.location = location;
	}
	
	public BankAccount() {
		balance = 1000;
		this.location = "FRANCE";
	}
	

	@Override
	public boolean credit(float value) {
		this.balance += value;
		return true; // Boolean pour si jamais un jour ce code servira 
	}// pour Rotschild ils pourront fixer des plafonds et retourner False afin de rester les plus riches
	

	@Override
	public boolean debit(float value, Creditable account) {
		
		if( this.balance < value)
			return false;
		
		if ( account.credit(value) ) {
			this.balance -= value;
			return true;
		}
		return false;
	}

	@Override
	public String getLocation() {
		return this.location;
	}

	public float getBalance() {
		return this.balance;
	}
	
	
}
