package engine.bank.network;

import engine.bank.Creditable;
import engine.bank.Debitable;

public interface Refundable {
	boolean refund(Creditable c, Debitable d, float value);
}
