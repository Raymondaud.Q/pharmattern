package engine.bank.network;

import engine.bank.BankAccount;
import engine.bank.Creditable;
import engine.bank.Debitable;
import engine.country.CountryFactory;

public class VisaNet implements Operable, Refundable {

	CountryFactory countryCostFactory;
	private static float tax = 0.0025f;
	private static BankAccount visaAccount = new BankAccount();
	
	public VisaNet() {
		 this.countryCostFactory = CountryFactory.getInstance();
	}
	
	@Override
	public boolean operate(Creditable c,Debitable d, float value) {
		// TODO Auto-generated method stub
		System.out.println("Taxe = " + value*tax);
		return ( d.debit(value, c)  && ((Debitable)c).debit(value*tax, visaAccount));
	}
	@Override
	public boolean refund( Creditable c, Debitable d, float value) {
		//{"SPAIN", "USA", "PORTUGAL", "UK", "SOMEWHERELSE"};
		
		float test = countryCostFactory.getVisaCost(c.getLocation(), value);
		System.out.println("Prix Refund = " + test);
		return ( d.debit(value, c)  && ((Debitable)c).debit(test, visaAccount)); 
	}

}
