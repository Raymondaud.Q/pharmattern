package engine.bank.network;

import engine.bank.Creditable;
import engine.bank.Debitable;

public interface Operable {
	boolean operate(Creditable c,Debitable d, float value);
}
