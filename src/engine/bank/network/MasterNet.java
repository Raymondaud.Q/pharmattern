package engine.bank.network;

import engine.bank.BankAccount;
import engine.bank.Creditable;
import engine.bank.Debitable;

public class MasterNet implements Operable {

	private static float tax = 0.005f;
	private static BankAccount masterAccount = new BankAccount();
	@Override
	public boolean operate(Creditable c, Debitable d, float value) {
		System.out.println("Taxe = " + value*tax);
		return ( d.debit(value, c)  && ((Debitable)c).debit(value*tax, masterAccount));
	}

}
