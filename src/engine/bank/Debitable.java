package engine.bank;

public interface Debitable {
	boolean debit(float value, Creditable account);
}
