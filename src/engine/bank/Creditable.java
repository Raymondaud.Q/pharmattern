package engine.bank;

public interface Creditable {
	boolean credit(float value );
	String getLocation();
}
