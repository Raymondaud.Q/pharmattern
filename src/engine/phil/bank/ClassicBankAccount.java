package engine.phil.bank;

public class ClassicBankAccount extends BankAccount {
	
	public ClassicBankAccount() {
	}
	
	public boolean debit(float amount) {
		// Il n'y a pas assez d'argent dans le compte.
		if (hasEnoughFunds(amount)) {
			return false;
		}
		
		_balance -= amount;
		
		return true;
	}
	
	public void credit(float amount) {
		_balance += amount;
	}

}
