package engine.phil.bank;

public abstract class BankAccount {
	
	protected float _balance;
	
	public float getBalance() {
		return _balance;
	}
	
	public BankAccount() {
		_balance = 100.0f;
	}
	
	public boolean hasEnoughFunds(float amount) {
		return (amount > _balance);
	}
	
	public abstract boolean debit(float amount);
	public abstract void credit(float amount);
	
}
