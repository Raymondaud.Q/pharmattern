package engine.phil.bank;

public class FranchisedBankAccount extends BankAccount {
	
	protected float _discount;
	
	public void setDiscount(float value) {
		_discount = value;
	}
	
	public FranchisedBankAccount() {
		super();
		
		_discount = 0.0f;
	}
	
	public FranchisedBankAccount(float discount) {
		super();
		
		_discount = discount;
	}
	
	public boolean debit(float amount) {
		// Appliquer le rabais de franchisť!
		amount *= (1.0f - _discount);
		
		// Il n'y a pas assez d'argent dans le compte.
		if (hasEnoughFunds(amount)) {
			return false;
		}
		
		_balance -= amount;
		
		return true;
	}
	
	public void credit(float amount) {
		// Appliquer l'inverse du rabais de franchisť.
		amount *= (1.0f - _discount);
		
		_balance += amount;
	}

}
