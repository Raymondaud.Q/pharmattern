package pharmatternApp;

import java.util.Calendar;
import java.util.UUID;
import java.util.Vector;

import controller.cli.CLIController;
import engine.bank.BankSystem;
import engine.bank.network.MasterNet;
import engine.bank.network.VisaNet;
import engine.country.CountryFactory;
import engine.employe.Address;
import engine.employe.Employee;
import engine.employe.OrderPicker;
import engine.employe.Pharmacist;
import engine.employe.Remunerable;
import engine.inventory.Inventory;
import engine.pharmacy.FranchisedPharmacy;
import engine.pharmacy.ParentFranchisedPharmacy;
import engine.pharmacy.Pharmacy;
import engine.pharmacy.PharmacyFactory;
import view.swing.SwingView;

public class Main {
	static SwingView swingView;
	public static void main(String[] args) {
		
		swingView = new SwingView();
		testBanking();
		testInventory();
		testPharmacyFactory();
		testEmployees();
		testCountryFlyFactory();
		testIntegration();
		System.out.println("\nType any unknowned command to exit CLI ! ");
		CLIController test = new CLIController();
		test.run();
	}
	
	
	private static void testBanking() {
		BankSystem bank = BankSystem.getInstance();
		bank.addNetwork("MASTER", new MasterNet());
		bank.addNetwork("VISA", new VisaNet());
		int id1 = bank.createAccount();
		int id2 = bank.createAccount();
		int id3 = bank.createAccount("UK");
		int id4 = bank.createAccount("USA");
		System.out.println(id1);
		
		bank.operate("VISA", ""+id1, ""+id2, 200);
		System.out.println(bank.getMoney(""+id1));
		System.out.println(bank.getMoney(""+id2));
		bank.operate("MASTER", ""+id1, ""+id2, 200);
		System.out.println(bank.getMoney(""+id1));
		System.out.println(bank.getMoney(""+id2));
		bank.refund("VISA", ""+id3, ""+id2, 200);
		System.out.println(bank.getMoney(""+id3));
		System.out.println(bank.getMoney(""+id2));
		bank.refund("VISA", ""+id4, ""+id3, 200);
		System.out.println(bank.getMoney(""+id4));
		System.out.println(bank.getMoney(""+id3));
		bank.refund("MASTER", ""+id4, ""+id3, 200);
		System.out.println(bank.getMoney(""+id4));
		System.out.println(bank.getMoney(""+id3));
	}

	private static void testCountryFlyFactory() {
		
		System.out.println("\n========== Countries Test =============");
		CountryFactory countryCostFactory = CountryFactory.getInstance();
		String[] testCountries = {"SPAIN", "USA", "PORTUGAL", "UK", "SOMEWHERELSE"};
		float testAmount = 100;
		float calculatedTaxAmount = 0.0f;
		for ( String paymentPlace : testCountries ) {
			float test = countryCostFactory.getVisaCost(paymentPlace, testAmount);
			System.out.println("PLACE = " + paymentPlace + " AMOUNT = " + testAmount + " VISA TAX = " +  test  );
		}
	}
	
	private static void testEmployees() {
		Vector<Employee> employees = new Vector<Employee>();
		Calendar cal = Calendar.getInstance();;
		for ( int i = 0 ; i < 10 ; i ++ ) {
			employees.add(new Pharmacist("AIMARRE", "JEAN", new Address("rue du Fondutrou", 5, "Avignoui", 84000),2500));
			if ( i  == 0 )
				cal.set(2013, 1, 1);
			else if ( i ==  1 )
				cal.set(2016, 1, 1);
			else if ( i  == 2 )
				cal.set(2020, 1, 1);
			employees.add(new OrderPicker("AIMARRE", "JEAN", new Address("rue du Fondutrou", 5, "Avignoui", 84000),9,35, cal.getTime()));
		}
		
		int cnt = 1 ;
		for ( Employee e : employees) {
			if (  e instanceof Pharmacist ) {
				for ( int i = 0 ; i < (cnt * 100) / 2 ; i++  )
					((Pharmacist) e).sell();
				cnt ++ ;
			}
		}
		
		for ( Remunerable r : employees) {
			System.out.println(r + " Salaire = " + r.getSalary());
		}
	}

	private static void testInventory() {
		BankSystem bank = BankSystem.getInstance();
		bank.addNetwork("MASTER", new MasterNet());
		bank.addNetwork("VISA", new VisaNet());
		
		Pharmacy p = PharmacyFactory.getInstance().getIndependantPharmacy("BigPharma Inc #1", "100000", 100.5f);
		p.setPrimaryBankAccountId(bank.createAccount());
		Inventory i = new Inventory();
		
		UUID orderId = null;
		String barcode = "abcdef";
		orderId = i.placeOrder(barcode, 5, p.getPrimaryBankAccount());
		orderId = i.placeOrder(barcode, 2, p.getPrimaryBankAccount());
		i.cancelOrder(orderId);

		barcode = "bcdefg";
		orderId = i.placeOrder(barcode, 3, p.getPrimaryBankAccount());
		i.sellProduct(barcode, 1, p.getPrimaryBankAccount());
		
		System.out.println(i);
	}
	
	private static void testIntegration() {
		BankSystem bank = BankSystem.getInstance();
		bank.addNetwork("MASTER", new MasterNet());
		bank.addNetwork("VISA", new VisaNet());
		
		Pharmacy p = PharmacyFactory.getInstance().getIndependantPharmacy("BigPharma Inc #1", "100000", 100.5f);
		p.setPrimaryBankAccountId(bank.createAccount());
		
		System.out.println(p);
		
		ParentFranchisedPharmacy fp1 = (ParentFranchisedPharmacy)PharmacyFactory.getInstance().getParentFranchisedPharmacy("BigPharma #1", "100100", 110f);
		fp1.setPrimaryBankAccountId(bank.createAccount());
		fp1.setFranchisedBankAccount(bank.createAccount());
		FranchisedPharmacy fp2 = PharmacyFactory.getInstance().getDependantFranchisedPharmacy("BiggerPharma #2", "100500", 200f);
		FranchisedPharmacy fp3 = PharmacyFactory.getInstance().getDependantFranchisedPharmacy("BiggestPharma #3", "100600", 300f);

		fp1.addDependant(fp2);
		fp1.addDependant(fp3);
//		fp1.addDependant(fp3);
		
		System.out.println(fp1);

		UUID orderId = null;
		String barcode = "abcdef";
		Inventory i = new Inventory();
		
		System.out.println();
		System.out.println("Inventaire (avant restock):");
		System.out.println(i);
		System.out.println(
			"Solde pharmacie - Classique (avant restock): " + 
			bank.getMoney(""+fp1.getPrimaryBankAccountId())
		);
		orderId = i.placeOrderById(barcode, 10, fp1.getPrimaryBankAccountId());
		UUID o2 = i.placeOrderById("defghi", 10, fp1.getPrimaryBankAccountId());

		System.out.println(
			"Solde pharmacie - Classique (apr�s restock): " + 
			bank.getMoney(""+fp1.getPrimaryBankAccountId())
		);
		
		System.out.println();
		System.out.println("Inventaire (apr�s restock):");
		System.out.println(i);

		i.cancelOrder(orderId);
		i.cancelOrder(o2);
		System.out.println(
			"Solde pharmacie - Classique (avant undo restock): " + 
			bank.getMoney(""+fp1.getPrimaryBankAccountId())
		);
		System.out.println();
		System.out.println("Inventaire (apr�s undo restock):");
		System.out.println(i);
		
		// Restocker pour l'achat du client!
		orderId = i.placeOrderById(barcode, 10, fp1.getPrimaryBankAccountId());
		System.out.println("** INVENTAIRE RESTOCK� POUR LE PROCHAIN TEST! **");
		System.out.println();
		System.out.println("Inventaire (avant achat):");
		System.out.println(i);
		
		int clientBankAccountId = BankSystem.getInstance().createAccount();
		System.out.println("Solde client (avant achat): " + bank.getMoney(""+clientBankAccountId));
		i.sellProductById(barcode, 2, clientBankAccountId);
		System.out.println("Solde client (apr�s achat): " + bank.getMoney(""+clientBankAccountId));

		System.out.println();
		System.out.println("Inventaire (post achat):");
		System.out.println(i);
	}
	
	private static void testPharmacyFactory() {
		BankSystem bank = BankSystem.getInstance();
		bank.addNetwork("MASTER", new MasterNet());
		bank.addNetwork("VISA", new VisaNet());
		
		Pharmacy p = PharmacyFactory.getInstance().getIndependantPharmacy("BigPharma Inc #1", "100000", 100.5f);
		p.setPrimaryBankAccountId(bank.createAccount());
		
		System.out.println(p);
		
		ParentFranchisedPharmacy fp1 = (ParentFranchisedPharmacy)PharmacyFactory.getInstance().getParentFranchisedPharmacy("BigPharma #1", "100100", 110f);
		fp1.setPrimaryBankAccountId(bank.createAccount());
		fp1.setFranchisedBankAccount(bank.createAccount());
		FranchisedPharmacy fp2 = PharmacyFactory.getInstance().getDependantFranchisedPharmacy("BiggerPharma #2", "100500", 200f);
		FranchisedPharmacy fp3 = PharmacyFactory.getInstance().getDependantFranchisedPharmacy("BiggestPharma #3", "100600", 300f);

		fp1.addDependant(fp2);
		fp1.addDependant(fp3);
//		fp1.addDependant(fp3);
		
		System.out.println(fp1);

		UUID orderId = null;
		String barcode = "abcdef";
		Inventory i = new Inventory();
		
		System.out.println(fp1);
		orderId = i.placeOrder(barcode, 10, fp1.getFranchisedBankAccount());
		i.placeOrder("efghij", 10, fp1.getPrimaryBankAccount());
		System.out.println(fp1);
		i.cancelOrder(orderId);
		System.out.println(fp1);
		
		System.out.println(i);
	}
}
