package view.swing;
import javax.swing.*;

import controller.mediator.Controller;
import controller.swing.CloseSwingAction;

import java.awt.*;

public class SwingView {
	
	//Usually you will require both swing and awt packages
	// even if you are working with just swings.
	JFrame frame;
	JMenuBar menuBar;
	JMenu viewOptions;
	JMenuItem onlyCLI;
	JPanel panel; 					// the panel is not visible in output
    JLabel label;
    JTextField tf; 					// accepts upto 20 characters
    JButton send;
    JButton reset;
    JTextArea ta = new JTextArea(); // Text Area at the Center
    Controller controle = new Controller();
	

	public SwingView() {
		//Creating the Frame
		this.frame = new JFrame("Pharmattern");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
		
		//Creating the MenuBar and adding components
		menuBar = new JMenuBar();
		viewOptions= new JMenu("Interface");
		menuBar.add(viewOptions);
		onlyCLI = new JMenuItem(new CloseSwingAction(frame));
		viewOptions.add(onlyCLI);
		
		//Creating the panel at bottom and adding components
		panel = new JPanel(); // the panel is not visible in output
		label = new JLabel("CMD");
		tf = new JTextField(15); // accepts upto 20 characters
		send = new JButton("Send");
		reset = new JButton("Reset");
		panel.add(label); // Components Added using Flow Layout
		panel.add(tf);
		panel.add(send);
		panel.add(reset);
		
		// Text Area at the Center
		JTextArea ta = new JTextArea();
		
		//Adding Components to the frame.
		frame.getContentPane().add(BorderLayout.SOUTH, panel);
		frame.getContentPane().add(BorderLayout.NORTH, menuBar);
		frame.getContentPane().add(BorderLayout.CENTER, ta);
		frame.setVisible(true);
	}

}
