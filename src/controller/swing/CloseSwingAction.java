package controller.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

public class CloseSwingAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	JFrame swingView;
	static String name = "Close";
	
	public CloseSwingAction(JFrame swing) {
		super(name);
		this.swingView = swing;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		this.swingView.dispose();
		
	}

}
