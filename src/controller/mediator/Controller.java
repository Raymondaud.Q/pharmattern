package controller.mediator;

import java.util.ArrayList;
import java.util.UUID;

import engine.bank.BankSystem;
import engine.bank.network.MasterNet;
import engine.bank.network.VisaNet;
import engine.inventory.Inventory;

//Mediator
public class Controller {
	BankSystem bank;
	Inventory i;
	ArrayList<Integer> accounts;
	ArrayList<UUID> orders;
	
	
	public Controller(){
		bank = BankSystem.getInstance();
		bank.addNetwork("MASTER", new MasterNet());
		bank.addNetwork("VISA", new VisaNet());
		i = new Inventory();
		accounts = new ArrayList<Integer>();
		orders = new ArrayList<UUID>();
		accounts.add(bank.createAccount());
		accounts.add(bank.createAccount("UK"));
	}
	
	public String order(String barcode, int qty) {
		UUID tmpID = i.placeOrderById(barcode, qty, 1);
		if ( tmpID != null && orders.add(tmpID) ) {
			return " Commande réussie : " + barcode + " * " + qty + 
					" Votre nouveau solde est de " + bank.getMoney(""+1);
		}
		return "Echec de la commande";
	}
	
	public String listOrders() {
		if  ( this.orders.size() == 0 )
			return "Vous n'avez aucune commande";
		String strOrders = " Order IDs : ";
		int cnt = 1;
		for ( UUID id : this.orders ) {
			strOrders += "\n		-N°"+cnt+" : " + id;
			cnt++;
		}
		return  strOrders;
	}
	
	public boolean cancelOrder(UUID id) {
		i.cancelOrder(id);
		return orders.remove(id);
	}
	
	public String pay(String opName, float value) {
		float cred1 = bank.getMoney(""+accounts.get(0));
		float deb1 = bank.getMoney(""+accounts.get(1));
		if ( bank.operate(opName, ""+accounts.get(0), ""+accounts.get(1), value) ) {
			return "Order valided : \n" + 
				   "Previous : cred = " + cred1 + "$  deb = " + deb1 +"$\n" +
				   "Current : cred = " + bank.getMoney(""+accounts.get(0)) + "$  deb = " + bank.getMoney(""+accounts.get(1)) +"$\n";
		};
		return "T'as plus une thune  ! ";
			
	}
	
}
