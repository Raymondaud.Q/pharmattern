package controller.cli;

import java.util.Scanner;
import java.util.UUID;

import controller.mediator.Controller;



public class CLIController {
	Controller ctrl;
	private Scanner scanner;
	
	public CLIController() {
		scanner = new Scanner(System.in);
		ctrl = new Controller();
	}
	
	public void run() {
		int res = 0;
		while(res != -1) 
			res = menu();	
	
	}
	
	public void printChoices() {
		System.out.println("Commandes dispo : ");
		System.out.println("		order < abcdef || bcdefg || ... > <int>");
		System.out.println("		pay  < MASTER || VISA > <int>");
		System.out.println("		listOrder || lo ");
		System.out.print("		cancelOrder <ID>\n $:");
	}
	
	
	public int menu() {

		this.printChoices();
		String input = scanner.nextLine();
		String[] tokens = input.split("\\s+");
		String firstPart = tokens[0];
		int intPart = 0;
		float value = 0.0f;
		if(firstPart.equals("order") || firstPart.equals("o")){
			if ( tokens.length < 3 ) {
				System.out.println("Commande erronée !");
				return 1;
			}
		    intPart = Integer.parseInt(tokens[2]);
		    System.out.println(ctrl.order(tokens[1], intPart));
		    return 1;
		}
		else if (  firstPart.equals("listOrder") || firstPart.equals("lo")) {
		    System.out.println(ctrl.listOrders());
		    return 1;
		}
		else if (  firstPart.equals("cancelOrder") || firstPart.equals("co")) {
			if ( tokens.length < 2 ) {
				System.out.println("Commande erronée !");
				return 1;
			}
		    if ( ctrl.cancelOrder(UUID.fromString(tokens[1])));
		    	System.out.println("Commande " + tokens[1] + " annulée !");
		    return 1;
		}
		else if (  firstPart.equals("pay") || firstPart.equals("p")) {
			if ( tokens.length < 3 ) {
				System.out.println("Commande erronée !");
				return 1;
			}
		    value = Float.parseFloat(tokens[2]);
		    System.out.println(ctrl.pay(tokens[1], value));
		    return 1;
		}
		else
		{
			System.out.println("EXIT");
		    return -1;
		}
	
	}
	

	
}
